/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.uepb.hibernateteste;

import br.edu.uepb.hibernateteste.entidade.Acompanhamento;
import br.edu.uepb.hibernateteste.entidade.Documento;
import br.edu.uepb.hibernateteste.entidade.Processo;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author aurelio
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernateTestePU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

//        Processo p = new Processo();
//        
//        ArrayList<Acompanhamento> acompanhamentos = new ArrayList<>();
//        acompanhamentos.add(new Acompanhamento("depacho1", p));
//        
//        ArrayList<Documento> documentos = new ArrayList<>();
//        documentos.add(new Documento("/jose/algumacoisa", p));
//        
//        p.setAcompanhamentos(acompanhamentos);
//        p.setDocumentos(documentos);
//        em.persist(p);
        Processo processoListado = em.find(Processo.class, 1l);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
